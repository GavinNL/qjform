cmake_minimum_required(VERSION 3.5)

project(QJForm LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

get_directory_property( qjform_is_subproject PARENT_DIRECTORY)


# QtCreator supports the following variables for Android, which are identical to qmake Android variables.
# Check http://doc.qt.io/qt-5/deployment-android.html for more information.
# They need to be set before the find_package(Qt5 ...) call.
if(qjform_is_subproject)
    option(QJFORM_BUILD_EXE   "Build the QJForm executable" FALSE)
    option(QJFORM_BUILD_TESTS "Build the QJForm tests"      FALSE)
else()
    option(QJFORM_BUILD_EXE   "Build the QJForm executable" TRUE)
    option(QJFORM_BUILD_TESTS "Build the QJForm tests"      TRUE)
endif()


find_package(Qt5 COMPONENTS Widgets REQUIRED)

add_library(QJForm_lib
    include/QJForm/qjform.h
    include/QJForm/ToggleSwitch.h
    include/QJForm/CollapsableWidget.h
)
target_include_directories( QJForm_lib PUBLIC include )

add_library( QJForm::QJForm ALIAS QJForm_lib  )
target_link_libraries(QJForm_lib PUBLIC Qt5::Widgets)

#######################################################################################
# Link this 'library' to use the warnings specified in CompilerWarnings.cmake
add_library(qjform_project_warnings INTERFACE)
include(cmake/CompilerWarnings.cmake)
set_project_warnings(qjform_project_warnings)
#######################################################################################

if( QJFORM_BUILD_TESTS )
add_subdirectory(test)
endif()
################################################################################
# cpp check
#
# Generate CPP check reports
#
#  make nd-cppcheck
#  make nd-cppcheck-report
#  xdg-open cppcheck/index.html
################################################################################
add_custom_target(qjform-cppcheck )

add_custom_command(TARGET qjform-cppcheck
    COMMAND echo "=================== CPPCHECK ===================="
    COMMAND mkdir -p ${CMAKE_BINARY_DIR}/cppcheck
    COMMAND cppcheck src -I include/ -ibuild -ibuild2 -ibuild3 --enable=all --inconclusive --xml-version=2 --force --library=windows,posix,gnu . --output-file=${CMAKE_BINARY_DIR}/cppcheck/result.xml
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}  # Need separate command for this line
    )

add_custom_target(qjform-cppcheck-report )

add_custom_command(TARGET qjform-cppcheck-report
    COMMAND echo "=================== CPPCHECK ===================="
    COMMAND mkdir -p ${CMAKE_BINARY_DIR}/cppcheck
    COMMAND cppcheck-htmlreport --source-encoding="iso8859-1" --title="qJForm" --source-dir . --report-dir=${CMAKE_BINARY_DIR}/cppcheck --file=${CMAKE_BINARY_DIR}/cppcheck/result.xml

    COMMAND echo "=================================================== "
    COMMAND echo " REPORT GENERATED: "
    COMMAND echo "=================================================== "
    COMMAND echo "  "
    COMMAND echo 'xdg-open ${CMAKE_BINARY_DIR}/cppcheck/index.html'
    COMMAND echo "  "
    COMMAND echo "=================================================== "
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}  # Need separate command for this line
    )
################################################################################

if( QJFORM_BUILD_EXE )
    add_executable(QJForm main.cpp )
    target_link_libraries(QJForm PRIVATE Qt5::Widgets QJForm::QJForm )
endif()
