#include <iostream>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMainWindow>
#include <QCommandLineParser>
#include <QJForm/qjform.h>
#include <cassert>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QWidget * m = new QWidget(nullptr);
    QFormLayout * f = new QFormLayout();
    m->setLayout(f);

    if(1){
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                                "type" : "object",
                                "ui:spacing" : 5,
                                "ui:margin" : 5,

                                "properties" : {
                                    "gtest" : {
                                        "type" : "object",
                                        "oneOf" : [
                                            {
                                                "type" : "object",
                                                "title" : "Name",
                                                "properties" : {
                                                    "firstName" : { "type" : "string" },
                                                    "lastName" : { "type" : "string" }
                                                }
                                            },
                                            {
                                                "type" : "object",
                                                "title" : "User ID",
                                                "properties" : {
                                                    "userID" : { "type" : "number" }
                                                }
                                            }
                                         ]
                                    }
                                }
                            }
                            )foo"));

        QJForm::QJObject * b = new QJForm::QJObject(J, nullptr, nullptr);

        f->addRow( b);

        b->show();
        {
            auto Jb = b->getValue();
            auto D = QJsonDocument( Jb.toObject() ).toJson();

            std::cout << D.toStdString() << std::endl;
        }
    }

    m->show();
    return a.exec();
}



