#include <iostream>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMainWindow>
#include <QCommandLineParser>
#include <QJForm/qjform.h>
#include <cassert>

bool QJFORM_ASSERT(bool cond)
{
    assert(cond);
    (void)cond;
    return cond;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QWidget * m = new QWidget(nullptr);
    QFormLayout * f = new QFormLayout();
    m->setLayout(f);


    if(0){
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                            "type" : "object",
                            "properties" : {
                                "number" : {
                                    "type" : "number",
                                    "title" : "Numerical"
                                },
                                "string" : {
                                    "type" : "string",
                                    "title" : "String Value"
                                },
                                "boolean" : {
                                    "type" : "boolean",
                                    "title" : "Boolean Value"
                                }
                            }
                            }
                            )foo"));

        QJForm::QJObject * b = new QJForm::QJObject(J, nullptr, nullptr);


        f->addRow( b);

        {
            auto Jb = b->getValue();
            auto D = QJsonDocument( Jb.toObject() ).toJson();

            std::cout << D.toStdString() << std::endl;
        }
    }


    if(1){
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                           "type" : "object",
                            "ui:spacing" : 1,
                            "ui:margin" : 5,
                            "properties" : {
                                "number" : {
                                    "type" : "number",
                                    "title" : "Hello",
                                    "minimum" : 5,
                                    "maximum" : 10,
                                    "ui:widget" : "range"
                                },
                                "string" : {
                                    "type" : "string"
                                },
                                "boolean" : {
                                    "type" : "boolean"
                                },
                                "color" : {
                                    "type" : "string",
                                    "default" : "#FF0000FF",
                                    "ui:widget" : "color"
                                },
                                "enum" : {
                                    "type" : "string",
                                    "enum" : ["wiz", "fight", "rng"],
                                    "enumNames" : ["Wizard", "Fighter", "Ranger"]
                                },
                                "array" : {
                                    "type" : "array",
                                    "items" : [
                                        {
                                            "type" : "number"
                                        },
                                        {
                                            "type" : "string"
                                        },
                                        {
                                            "type" : "boolean"
                                        }
                                    ]
                                },
                                "object" : {
                                    "type" : "object",
                                    "properties" : {
                                        "number" : {
                                            "type" : "number"
                                        },
                                        "string" : {
                                            "type" : "string"
                                        },
                                        "boolean" : {
                                            "type" : "boolean"
                                        },
                                        "array" : {
                                            "type" : "array",
                                            "items" : [
                                                {
                                                    "type" : "number"
                                                },
                                                {
                                                    "type" : "string"
                                                },
                                                {
                                                    "type" : "boolean",
                                                    "ui:widget" : "switch"
                                                },
                                                {
                                                    "type" : "object",
                                                    "properties" : {
                                                        "firstName" : {
                                                            "type" : "string",
                                                            "title": "First Name"
                                                        },
                                                        "lastName" : {
                                                            "type" : "string",
                                                            "title": "Last Name"
                                                        }
                                                    }
                                                }
                                            ]
                                        }
                                    }
                                }
                            }
                            }
                            )foo"));

        QJForm::QJObject * b = new QJForm::QJObject(J, nullptr, nullptr);
        QJForm::QJObject * c = new QJForm::QJObject(J, nullptr, nullptr);

        b->connect(b, &QJForm::QJObject::valueChanged,
                   [c,b](QString const & p)
        {
            auto v = b->findChildRecursively(p)->getValue();
            auto x = c->findChildRecursively(p);

            if( x )
            {
                x->setValue(v);
            }
           std::cout << p.toStdString() << std::endl;
        });

        {
            auto boo = b->getItem("boolean");
            QJFORM_ASSERT(boo->getPath() == "/boolean");
            QJFORM_ASSERT( boo != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJBoolean*>(boo) != nullptr );
        }

        {
            auto boo = b->getItem("boolean");
            QJFORM_ASSERT(boo->getPath() == "/boolean");
            QJFORM_ASSERT( boo != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJBoolean*>(boo) != nullptr );
        }
        {
            auto boo = b->getItem("number");
            QJFORM_ASSERT(boo->getPath() == "/number");
            QJFORM_ASSERT( boo != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJNumber*>(boo) != nullptr );
        }
        {
            auto boo = b->getItem("string");
            QJFORM_ASSERT(boo->getPath() == "/string");
            QJFORM_ASSERT( boo != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJString*>(boo) != nullptr );
        }
        {
            auto boo = b->getItem("array");
            QJFORM_ASSERT(boo->getPath() == "/array");
            QJFORM_ASSERT( boo != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJArray*>(boo) != nullptr );
        }
        {
            auto boo = b->getItem("object");
            QJFORM_ASSERT(boo->getPath() == "/object");
            QJFORM_ASSERT( boo != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJObject*>(boo) != nullptr );
            auto ob = dynamic_cast<QJForm::QJObject*>(boo);

            auto root = ob->getRootParent();
            QJFORM_ASSERT( root == b);

        }
        {
            auto boo = b->findChildRecursively("object/array/2");
            auto path = boo->getPath();
            QJFORM_ASSERT( path == "/object/array/2");
            QJFORM_ASSERT( boo != nullptr);
            QJFORM_ASSERT( dynamic_cast<QJForm::QJBoolean*>(boo) != nullptr );
        }
        {
            auto boo = b->findChildRecursively("object/array/3/firstName");
            QJFORM_ASSERT(boo->getPath() == "/object/array/3/firstName");
            QJFORM_ASSERT( boo != nullptr);
            QJFORM_ASSERT( dynamic_cast<QJForm::QJString*>(boo) != nullptr );
        }
        {
            auto boo = b->findChildRecursively("object/array/3/lastName");
            std::cout << boo->getPath().toStdString() << "--" << std::endl;
            QJFORM_ASSERT( boo->getPath() == "/object/array/3/lastName");
            QJFORM_ASSERT( boo != nullptr);
            QJFORM_ASSERT( dynamic_cast<QJForm::QJString*>(boo) != nullptr );
        }
        f->addRow( b);
//        /c->show();
        auto d = new QJForm::CollapsableWidget();
        (void)d->setWidget(c);
        d->show();
        {
            auto Jb = b->getValue();
            auto D = QJsonDocument( Jb.toObject() ).toJson();

            std::cout << D.toStdString() << std::endl;
        }
    }

    m->show();
    return a.exec();
}



