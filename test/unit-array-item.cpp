#include <iostream>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMainWindow>
#include <QCommandLineParser>
#include <QJForm/qjform.h>
#include <cassert>

bool QJFORM_ASSERT(bool cond)
{
    assert(cond);
    (void)cond;
    return cond;
}


// This tests the QJArrayItem->takeCentralWidget
// and setCentralWidget methods
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    auto b = new QPushButton();
    auto bb = new QJForm::QJArrayItem(b , nullptr );

    b->connect( b, &QPushButton::pressed, [&]
    ()
    {
        auto b1 = bb->takeCentralWidget();
       QJFORM_ASSERT( b ==  b1);

       bb->setCentralWidget( new QLineEdit());
       b1->setParent(nullptr);
       b1->show();
       b1->deleteLater();
    });
    bb->show();

    return a.exec();
}



