#include <iostream>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMainWindow>
#include <QCommandLineParser>
#include <QJForm/qjform.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QWidget * m = new QWidget(nullptr);
    QFormLayout * f = new QFormLayout();
    m->setLayout(f);


    {
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                            "type" : "string",
                            "default" : true
                            }
                            )foo"));

        QJForm::QJString * b = new QJForm::QJString(J, nullptr, nullptr);

        f->addRow("default", b);
    }

    {
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                            "type" : "string",
                            "default" : "hello"
                            }
                            )foo"));

        QJForm::QJString * b = new QJForm::QJString(J, nullptr, nullptr);
        f->addRow("switch", b);
    }

    {
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                            "type" : "string",
                            "ui:widget" : "color"
                            }
                            )foo"));

        QJForm::QJString * b = new QJForm::QJString(J, nullptr, nullptr);
        f->addRow("color", b);
    }

    {
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                            "type" : "string",
                            "ui:widget" : "file"
                            }
                            )foo"));

        QJForm::QJString * b = new QJForm::QJString(J, nullptr, nullptr);
        f->addRow("file", b);
    }
    m->show();

    return a.exec();
}



