#include <iostream>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMainWindow>
#include <QCommandLineParser>
#include <QJForm/qjform.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QWidget * m = new QWidget(nullptr);
    QFormLayout * f = new QFormLayout();
    m->setLayout(f);


    {
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                                "type" : "number",
                                "default" : 155,
                                "description" : "This is a standard number"
                            }
                            )foo"));

        QJForm::QJNumber * b = new QJForm::QJNumber(J, nullptr, nullptr);

        f->addRow("default", b);
    }

    {
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                                "type" : "number",
                                "default" : 3.1545,
                                "description" : "This is a float number"
                            }
                            )foo"));

        QJForm::QJNumber * b = new QJForm::QJNumber(J, nullptr, nullptr);
        f->addRow("switch", b);
    }

    {
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                                "type" : "number",
                                "ui:widget" : "range",
                                "default" : 3.1545,
                                "description" : "This is a range number"
                            }
                            )foo"));

        QJForm::QJNumber * b = new QJForm::QJNumber(J, nullptr, nullptr);
        f->addRow("range", b);
    }

    m->show();
    return a.exec();
}



