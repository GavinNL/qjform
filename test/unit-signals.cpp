#include <iostream>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMainWindow>
#include <QCommandLineParser>
#include <QJForm/qjform.h>
#include <cassert>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QWidget * m = new QWidget(nullptr);
    QFormLayout * f = new QFormLayout();
    m->setLayout(f);

    QJsonObject J = QJForm::FromString(
                QString(R"foo(
                        {
                        "type" : "object",
                        "ui:spacing" : 1,
                        "ui:margin" : 5,
                        "properties" : {
                            "enable" : {
                                "type" : "boolean",
                                "ui:widget" : "switch",
                                "default" : true
                            },
                            "object" : {
                                "ui:widget" : "window",
                                "type" : "object",
                                "properties" : {
                                    "number" : {
                                        "type" : "number"
                                    },
                                    "string" : {
                                        "type" : "string"
                                    },
                                    "boolean" : {
                                        "type" : "boolean"
                                    }
                                }
                            }
                        }
                        }
                        )foo"));

        QJForm::QJObject * b = new QJForm::QJObject(J, nullptr, nullptr);

#if 1
        b->connect(b, &QJForm::QJObject::widgetValueChanged,
                   [b](QJForm::QJBase * obj)
        {
            if( obj->getPath() == "/enable" )
            {
                auto val = obj->getValue().toBool();
                auto c = b->findChildRecursively("/object");
                if(c)
                {
                    c->setEnabled(val);
                }

            }
        });
#else
        b->connect(b, &QJForm::QJObject::valueChanged,
                   [b](QString const & path)
        {
            if( path == "/enable")
            {
                auto val = b->findChildRecursively(path)->getValue().toBool();
                auto c = b->findChildRecursively("/object");
                if(c)
                {
                    c->setEnabled(val);
                }
            }
        });
#endif
        f->addRow( b);

    m->show();
    return a.exec();
}



