#include <iostream>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMainWindow>
#include <QCommandLineParser>
#include <QJForm/qjform.h>
#include <cassert>

bool QJFORM_ASSERT(bool cond)
{
    assert(cond);
    (void)cond;
    return cond;
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QWidget * m = new QWidget(nullptr);
    QHBoxLayout * h = new QHBoxLayout();
    m->setLayout(h);
    QVBoxLayout * f0 = new QVBoxLayout();
    QVBoxLayout * f = new QVBoxLayout();
    h->insertLayout(0,f0);
    h->insertLayout(1,f);


    QJForm::QJArray * additionalItems = nullptr;

    {
        QJForm::CollapsableWidget * c = new QJForm::CollapsableWidget();
        c->setText("Additional Items");
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                            "type" : "array",
                            "items" : [
                                {
                                    "type" : "number",
                                    "default" : 3.14159
                                },
                                {
                                    "type" : "string",
                                    "default" : "hello"
                                },
                                {
                                    "type" : "boolean",
                                    "default" : true,
                                    "ui:widget" : "switch"
                                },
                                {
                                    "type" : "boolean",
                                    "default" : true
                                }
                            ],
                            "additionalItems" : {
                                "type" : "boolean",
                                "default" : true,
                                "ui:widget" : "switch"
                            }
                            }
                            )foo"));

        QJForm::QJArray * b = new QJForm::QJArray(J, nullptr, nullptr);
        additionalItems = b;
        (void)c->setWidget(b);

        {
            auto boo  = b->getItem(0);
            auto boos = b->getItem("0");
            QJFORM_ASSERT( boo != nullptr );
            QJFORM_ASSERT( boos != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJNumber*>(boo) != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJNumber*>(boos) != nullptr );
        }
        {
            auto boo  = b->getItem(1);
            auto boos = b->getItem("1");
            QJFORM_ASSERT( boo != nullptr );
            QJFORM_ASSERT( boos != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJString*>(boo) != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJString*>(boos) != nullptr );
        }
        {
            auto boo  = b->getItem(2);
            auto boos = b->getItem("2");
            QJFORM_ASSERT( boo != nullptr );
            QJFORM_ASSERT( boos != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJBoolean*>(boo) != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJBoolean*>(boos) != nullptr );
        }
        {
            auto boo  = b->getItem(4);
            auto boos = b->getItem("4");
            QJFORM_ASSERT( boo  == nullptr );
            QJFORM_ASSERT( boos == nullptr );
        }

        f->addWidget( c);

        {
            auto Jb = b->getValue();
            auto D = QJsonDocument( Jb.toArray() ).toJson();

            std::cout << D.toStdString() << std::endl;
        }
        m->show();
    }

    {
        auto b = new QPushButton("test");

        b->connect( b, &QPushButton::pressed,
                    [&]()
        {
            {
                std::cout << "asdfasdf" << std::endl;
                QJsonObject J = QJForm::FromString(
                            QString(R"foo(
                                    {
                                        "items" : [ true, false, true, true]
                                    }
                                    )foo"));
                additionalItems->setValue( J["items"]);

            }


        });

        f0->addWidget( b );
    }



    {
        QJForm::CollapsableWidget * c = new QJForm::CollapsableWidget();
        c->setText("Fixed Length");

        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                            "type" : "array",
                            "items" : [
                                {
                                    "type" : "number",
                                    "default" : 3.14159
                                },
                                {
                                    "type" : "string",
                                    "default" : "hello"
                                },
                                {
                                    "type" : "boolean",
                                    "default" : true,
                                    "ui:widget" : "switch"
                                },
                                {
                                    "type" : "boolean",
                                    "default" : true
                                }
                            ]
                            }
                            )foo"));

        QJForm::QJArray * b = new QJForm::QJArray(J, nullptr, nullptr);
        (void)c->setWidget(b);

        {
            QJFORM_ASSERT( b->itemCount() == 4);
        }
        {
            auto boo  = b->getItem(0);
            auto boos = b->getItem("0");
            QJFORM_ASSERT( boo != nullptr );
            QJFORM_ASSERT( boos != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJNumber*>(boo) != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJNumber*>(boos) != nullptr );
        }
        {
            auto boo  = b->getItem(1);
            auto boos = b->getItem("1");
            QJFORM_ASSERT( boo != nullptr );
            QJFORM_ASSERT( boos != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJString*>(boo) != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJString*>(boos) != nullptr );
        }
        {
            auto boo  = b->getItem(2);
            auto boos = b->getItem("2");
            QJFORM_ASSERT( boo != nullptr );
            QJFORM_ASSERT( boos != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJBoolean*>(boo) != nullptr );
            QJFORM_ASSERT( dynamic_cast<QJForm::QJBoolean*>(boos) != nullptr );
        }
        {
            auto boo  = b->getItem(4);
            auto boos = b->getItem("4");
            QJFORM_ASSERT( boo  == nullptr );
            QJFORM_ASSERT( boos == nullptr );
        }

        f->addWidget( c);

        {
            auto Jb = b->getValue();
            auto D = QJsonDocument( Jb.toArray() ).toJson();

            std::cout << D.toStdString() << std::endl;
        }
        m->show();
    }

    f->addStretch(1);
    return a.exec();
}



