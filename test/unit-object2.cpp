#include <iostream>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMainWindow>
#include <QCommandLineParser>
#include <QJForm/qjform.h>
#include <cassert>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QWidget * m = new QWidget(nullptr);
    QFormLayout * f = new QFormLayout();
    m->setLayout(f);

    if(1){
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                                "type" : "object",
                                "ui:spacing" : 5,
                                "ui:margin" : 5,
                                "ui:order" : ["gtest", "race", "class"],
                                "properties" : {
                                    "gtest" : {
                                        "type" : "number"
                                    },
                                    "race" : {
                                        "ui:widget" : "window",
                                        "type" : "object",
                                        "title" : "Race and Alignment",
                                        "properties" : {
                                            "race" : {
                                                "title" : "Race",
                                                "type" : "string",
                                                "enum" : ["Human", "Gnome", "Halfling", "Half-Orc", "Elf", "Half-Elf", "Goliath"]
                                            },
                                            "alignment_a" : {
                                                "title" : "Alignment",
                                                "type" : "string",
                                                "enum" : ["Lawful", "Neutral", "Chaotic"]
                                            },
                                            "alignment_b" : {
                                                "title" : "Alignment",
                                                "type" : "string",
                                                "enum" : ["Good", "Neutral", "Evil"]
                                            }
                                        },
                                        "ui:order" : ["race", "alignment_a", "alignment_b"]
                                    },

                                    "class" : {
                                        "ui:widget" : "window",
                                        "type" : "object",
                                        "title" : "Class",
                                        "properties" : {
                                            "class" : {
                                                "title" : "Main Class",
                                                "type" : "string",
                                                "enum" : ["Wizard", "Fighter", "Barbarian", "Warlock", "Cleric", "Bard", "Ranger", "Rogue", "Sorcerer"]
                                            },
                                            "subclass" : {
                                                "title" : "Sub-Class",
                                                "type" : "string"
                                            },
                                            "class" : {
                                                "ui:widget" : "window",
                                                "type" : "object",
                                                "title" : "Class",
                                                "properties" : {
                                                    "class" : {
                                                        "title" : "Main Class",
                                                        "type" : "string",
                                                        "enum" : ["Wizard", "Fighter", "Barbarian", "Warlock", "Cleric", "Bard", "Ranger", "Rogue", "Sorcerer"]
                                                    },
                                                    "subclass" : {
                                                        "title" : "Sub-Class",
                                                        "type" : "string"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            )foo"));

        QJForm::QJObject * b = new QJForm::QJObject(J, nullptr, nullptr);

        f->addRow( b);

        b->show();
        {
            auto Jb = b->getValue();
            auto D = QJsonDocument( Jb.toObject() ).toJson();

            std::cout << D.toStdString() << std::endl;
        }
    }

    m->show();
    return a.exec();
}



