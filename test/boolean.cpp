#include <iostream>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMainWindow>
#include <QCommandLineParser>
#include <QJForm/qjform.h>

#if 1

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QWidget * m = new QWidget(nullptr);
    QFormLayout * f = new QFormLayout();
    m->setLayout(f);


    {
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                            "type" : "boolean",
                            "default" : true,
                            "ui:widget" : "switch"
                            }
                            )foo"));

        QJForm::QJBoolean * b = new QJForm::QJBoolean(J, nullptr, nullptr);

        f->addRow("default", b);
    }

    {
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                            "type" : "boolean",
                            "default" : true
                            }
                            )foo"));

        QJForm::QJBoolean * b = new QJForm::QJBoolean(J, nullptr, nullptr);
        f->addRow("switch", b);
    }
    m->show();
    return a.exec();
}

#endif

