#include <iostream>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMainWindow>
#include <QCommandLineParser>
#include <QJForm/qjform.h>
#include <cassert>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    {
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                            "type" : "array",
                            "items" : [
                                {
                                    "type" : "number",
                                    "default" : 3.14159
                                },
                                {
                                    "type" : "string",
                                    "default" : "hello"
                                },

                                {
                                    "type" : "boolean",
                                    "default" : true
                                }
                            ],
                            "additionalItems" : {
                                "oneOf" : [
                                            {
                                                "title" : "Number",
                                                "type" : "number",
                                                "default" : 3.14159
                                            },
                                            {
                                                "title" : "Text",
                                                "type" : "string",
                                                "default" : "hello"
                                            },

                                            {
                                                "title" : "Checkbox Bool",
                                                "type" : "boolean",
                                                "description" : "Hello this is a test",
                                                "default" : true
                                            },
                                            {
                                                "type" : "boolean",
                                                "title" : "Switch Bool",
                                                "default" : true,
                                                "description" : "Hello this is a test",
                                                "ui:widget" : "switch"
                                            }
                                        ]
                            }
                            }
                            )foo"));

        QJForm::QJArray * b = new QJForm::QJArray(J, nullptr, nullptr);

        b->show();

    }

    return a.exec();
}



