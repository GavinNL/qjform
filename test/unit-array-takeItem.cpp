#include <iostream>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMainWindow>
#include <QCommandLineParser>
#include <QJForm/qjform.h>
#include <cassert>

// this test checks the QJArray::takeItemAt method
// taking an item makes qt no longer manage it.
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QWidget * m = new QWidget(nullptr);
    QHBoxLayout * h = new QHBoxLayout();
    m->setLayout(h);
    QVBoxLayout * f0 = new QVBoxLayout();
    QVBoxLayout * f = new QVBoxLayout();
    h->insertLayout(0,f0);
    h->insertLayout(1,f);


    QJForm::QJArray * additionalItems = nullptr;

    {
        QJForm::CollapsableWidget * c = new QJForm::CollapsableWidget();
        c->setText("Additional Items");
        QJsonObject J = QJForm::FromString(
                    QString(R"foo(
                            {
                            "type" : "array",
                            "items" : [
                                {
                                    "type" : "number",
                                    "default" : 3.14159
                                },
                                {
                                    "type" : "string",
                                    "default" : "hello"
                                },
                                {
                                    "type" : "boolean",
                                    "default" : true,
                                    "ui:widget" : "switch"
                                },
                                {
                                    "type" : "boolean",
                                    "default" : true
                                }
                            ],
                            "additionalItems" : {
                                "type" : "boolean",
                                "default" : true
                            }
                            }
                            )foo"));

        QJForm::QJArray * b = new QJForm::QJArray(J, nullptr, nullptr);
        additionalItems = b;
        (void)c->setWidget(b);

        f->addWidget( c);

        m->show();
    }

    {
        auto b = new QPushButton("Take Last Item");
        b->connect( b, &QPushButton::pressed, [&]()
        {
            auto c = additionalItems->takeItemAt( additionalItems->itemCount()-1);
            c->setParent(nullptr);
            c->show();
            f0->addWidget(c);
        });
        f0->addWidget(b);
    }

    m->show();
    f->addStretch(1);
    return a.exec();
}



