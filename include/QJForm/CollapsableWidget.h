#ifndef QJFORM_COLLAPSABLEWIDGET_H
#define QJFORM_COLLAPSABLEWIDGET_H

#include <QToolButton>
#include <QPushButton>
#include <QVBoxLayout>
#include <QString>

namespace QJForm
{


class CollapsableWidget : public QWidget
{
Q_OBJECT
public:
    explicit CollapsableWidget(QString const & text, QWidget* parent=nullptr);
    CollapsableWidget(QWidget* parent=nullptr);

    /**
     * @brief setText
     * @parama t
     *
     * Sets the text of the bar
     */
    void setText( QString const &t);

    /**
     * @brief text
     * @return
     *
     * Gets the text of the bar
     */
    QString text() const;

    /**
     * @brief widget
     * @return
     *
     * Gets the central widet
     */
    QWidget* widget();

    /**
     * @brief takeWidget
     * @return
     *
     * Removes the central widget and returns it.
     * You are now responsible for deleting it.
     */
    [[ nodiscard ]] QWidget* takeWidget();

    /**
     * @brief setWidget
     * @param p
     *
     * Sets the central widget and returns the old one.
     * You are responsible for deleting the previous widget.
     */
    [[ nodiscard ]] QWidget* setWidget(QWidget* p);

signals:
    void toggled(bool b);

public slots:
    void toggle(bool b);

private:
    QGridLayout * m_layout = nullptr;
    QVBoxLayout * m_WidgetLayout= nullptr;
    QToolButton * m_button = nullptr;
    QPushButton * m_closeButton = nullptr;
    QWidget     * m_centralWidget = nullptr;
};

inline QJForm::CollapsableWidget::CollapsableWidget(const QString &text, QWidget *parent) : CollapsableWidget(parent)
{
    setText(text);
}

inline QJForm::CollapsableWidget::CollapsableWidget(QWidget *parent) : QWidget(parent)
{
    auto buttonLayout = new QHBoxLayout();
    m_layout = new QGridLayout();

    m_WidgetLayout = new QVBoxLayout();
    m_WidgetLayout->setContentsMargins(15,5,15,0);

    m_button = new QToolButton();
    m_button->setCheckable(true);
    m_button->setChecked(true);


    m_button->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    m_button->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
    m_button->setArrowType(Qt::ArrowType::DownArrow);
    m_button->setStyleSheet( QString(
                                 "QToolButton {"
                                 "background-color: rgb(85, 85, 85);"
                                 "border-style: outset;"
                                 "border-width: 0px;   "
                                 "border-color: beige; "
                                 "}"
                                 "QToolButton::pressed {"
                                 "background-color: rgb(85, 85, 85);"
                                 "border-width: 0px;   "
                                 "}"
                                 "QToolButton::checked {"
                                 "background-color: rgb(85, 85, 85);"
                                 "border-width: 0px;   "
                                 "}"
                                 ) );

    m_button->setLayout( new QHBoxLayout() );
    m_button->setMinimumHeight(20);

    m_closeButton = new QPushButton();
    m_closeButton->setMaximumHeight(20);
    m_closeButton->setMinimumWidth(20);
    m_closeButton->setMaximumWidth(20);
    m_closeButton->setText("✖");
    m_closeButton->setMinimumHeight(20);
    m_closeButton->setStyleSheet( QString(
                                 "QPushButton {"
                                 "background-color: rgb(85, 85, 85);"
                                 "border-style: outset;"
                                 "border-width: 0px;   "
                                 "border-color: beige; "
                                 "}"
                                 "QPushButton::pressed {"
                                 "background-color: rgb(85, 85, 85);"
                                 "border-width: 0px;   "
                                 "}"
                                 "QPushButton::checked {"
                                 "background-color: rgb(85, 85, 85);"
                                 "border-width: 0px;   "
                                 "}"
                                 ) );


    buttonLayout->setMargin(  0);
    buttonLayout->setSpacing( 0 );

    m_layout->setMargin(  0);
    m_layout->setSpacing( 3 );

    buttonLayout->addWidget(m_button);
    buttonLayout->addWidget(m_closeButton);

    m_layout->addLayout(buttonLayout,0,0);
    m_layout->addLayout(m_WidgetLayout,1,0);

    this->setLayout( m_layout );

    m_button->connect( m_button, &QToolButton::toggled, [this](bool togg)
    {
        m_button->setArrowType(togg ? Qt::ArrowType::DownArrow : Qt::ArrowType::RightArrow);
        if(m_centralWidget)
        {
            if(!togg)
            {
                m_layout->takeAt(1);
                m_WidgetLayout->takeAt(0);
            }
            else
            {
                m_layout->addLayout(m_WidgetLayout,1,0);
                m_WidgetLayout->addWidget(m_centralWidget);
            }
            m_centralWidget->setVisible(togg);
        }
        emit toggled(togg);
    });
}

inline void QJForm::CollapsableWidget::setText(const QString &t)
{
    m_button->setText(t);
}

inline QString QJForm::CollapsableWidget::text() const
{
    return m_button->text();
}

inline QWidget *QJForm::CollapsableWidget::widget()
{
    return m_centralWidget;
}

inline QWidget *QJForm::CollapsableWidget::takeWidget()
{
    auto L = m_WidgetLayout->takeAt(0);
    delete L;
    auto p = m_centralWidget;
    m_centralWidget=nullptr;
    return p;
}

inline QWidget* QJForm::CollapsableWidget::setWidget(QWidget *p)
{
    auto old = takeWidget();

    m_centralWidget = p;
    m_WidgetLayout->addWidget(p);
    return old;
}

inline void QJForm::CollapsableWidget::toggle(bool t)
{
    m_button->setChecked(t);
}


}

#endif
