#ifndef QJFORM2_STRING_H
#define QJFORM2_STRING_H

#include <QGroupBox>
#include <QWidget>
#include <QDoubleSpinBox>
#include <QSlider>
#include <QLineEdit>
#include <QPushButton>
#include <QComboBox>
#include <QTabWidget>
#include <QToolButton>
#include <QLabel>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonDocument>
#include <QScrollArea>
#include <QFormLayout>
#include <QCheckBox>
#include <QListWidget>
#include <QDateEdit>
#include <QColorDialog>
#include <QFileDialog>

#include "CollapsableWidget.h"
#include "ToggleSwitch.h"
#include <variant>
#include <iostream>
#include <set>
#include <cassert>

#define QJFORM_VERSION_MAJOR 0
#define QJFORM_VERSION_MINOR 2

#define QJFORM_VERSION "0.2"


namespace QJForm
{


class QJValue;
class QJForm;
class QJObject;
class QJArray;
class QJString;
class QJNumber;
class QJBoolean;
class QJBase;

QJBase* takeQJBase(QWidget * w);
QJBase* getQJBase(QWidget * w);

inline QJsonObject FromString(const QString& in)
{
    QJsonObject obj;

    QJsonDocument doc = QJsonDocument::fromJson(in.toUtf8());

    // check validity of the document
    if(!doc.isNull())
    {
        if(doc.isObject())
        {
            obj = doc.object();
        }
        else
        {
            throw std::runtime_error("Failed to parse");
            return {};
        }
    }
    else
    {
        throw std::runtime_error("Failed to parse");
        return {};
    }

    return obj;
}

/**
 * @brief The QJBase class
 *
 * The base class for all JSON Schema widgets
 */
class QJBase : public QWidget
{
public:
    explicit QJBase(QWidget *parent, QJForm *parentForm) : QWidget(parent), m_parentForm(parentForm)
    {

    }

    /**
     * @brief setSchema
     * @param J
     *
     * Sets the schema for the widget. This method
     * also constructs the widget
     */
    virtual void setSchema(const QJsonObject & J) = 0;

    /**
     * @brief getValue
     * @return
     *
     * Returns the value of the JSON Widget as a json value
     */
    virtual QJsonValue getValue() const = 0;

    /**
     * @brief setValue
     * @param V
     *
     * Sets the value of the widget
     */
    virtual void setValue(QJsonValue const &V) = 0;


    virtual void setDescription(QString const & S)
    {
        (void)S;
    }

    QJForm* getParentForm()
    {
        return m_parentForm;
    }
    QJForm const * getParentForm() const
    {
        return m_parentForm;
    }

    void setProperty(QString const & s)
    {
        m_propertyLabel = s;
    }
    QString const & getPropertyLabel() const
    {
        return m_propertyLabel;
    }

    void setParent(QJBase * mp)
    {
        m_parent = mp;
    }
    QJBase* getParent()
    {
        return m_parent;
    }
    QJBase const * getParent() const
    {
        return m_parent;
    }

    QString getPath() const
    {
        auto p = getParent();
        if( p )
        {
            return getParent()->getPath() + "/" + getPropertyLabel();
        }
        return getPropertyLabel();
    }
protected:
    QJForm *m_parentForm = nullptr;
    QString m_propertyLabel;
    QJBase* m_parent=nullptr;
};


/**
 * @brief The QJBoolean class
 *
 * Top level class for boolean controls ( checkbox/switches)
 */
class QJBoolean : public QJBase
{
    Q_OBJECT
public:

    enum Style
    {
        DEFAULT=0,
        SWITCH=1,
        UNKNOWN=255
    };

    explicit QJBoolean(QWidget* parent, QJForm* parentForm=nullptr) : QJBase(parent, parentForm)
    {
        auto l = new QHBoxLayout();
        this->setLayout(l);
        l->setSpacing(0);
        l->setMargin(0);
        setStyle(Style::DEFAULT);
    }

    QJBoolean(QJsonObject schema, QWidget* parent, QJForm* parentForm=nullptr) : QJBase(parent, parentForm)
    {
        auto l = new QHBoxLayout();
        this->setLayout(l);
        l->setSpacing(0);
        l->setMargin(0);
        QJBoolean::setSchema(schema);
    }

    Style getStyle() const
    {
        return m_style;
    }

    void setStyle( Style s)
    {
        if( getStyle() == s)
            return;
        _deleteOldWidget();
        m_style = s;

        switch(s)
        {
            case DEFAULT:
            {
                auto cb = new QCheckBox(this);
                cb->setObjectName("QJBoolean::QCheckBox");
                cb->setLayoutDirection(Qt::RightToLeft);
                dynamic_cast<QHBoxLayout*>(layout())->addWidget(cb);
                m_widget = cb;
                cb->connect(cb, &QCheckBox::toggled,
                            [this](bool d)
                {
                    emit this->valueChanged(d);
                });
                break;
            }
            case SWITCH:
            {
                auto cb = new ToggleSwitch(10,12,this);
                cb->setObjectName("QJBoolean::ToggleSwitch");
                dynamic_cast<QHBoxLayout*>(layout())->addWidget(new QWidget());
                dynamic_cast<QHBoxLayout*>(layout())->addWidget(cb);
                m_widget = cb;

                cb->connect(cb, &ToggleSwitch::toggled,
                            [this](bool d)
                {
                    emit this->valueChanged(d);
                });
                break;
            }
            case UNKNOWN:
                throw std::runtime_error("asdasdfs");
            break;
        }
    }

    void setSchema(const QJsonObject & J) override
    {
        auto mI = J.find("ui:widget");
        QString wid = "default";
        if( mI != J.end() )
        {
            wid = mI->toString();
        }

        QString desc = "";
        mI = J.find("description");
        if( mI != J.end())
            desc = mI->toString();

        if( wid == "default")
        {
            setStyle(Style::DEFAULT);
        }
        else if( wid == "switch")
        {
            setStyle(Style::SWITCH);
        }

        auto md = J.find("default");
        if( md != J.end())
        {
            set( md->toBool());
        }

        setDescription(desc);
    }

    void setValue( QJsonValue const & v) override
    {
        if( !v.isBool() )
            return;

        setValue(v.toBool());
    }

    void setDescription(QString const & S) override
    {
        std::visit([&](auto&& arg)
        {
            using T = std::decay_t<decltype(arg)>;
            if constexpr (std::is_same_v<T, QCheckBox*>)
            {
                arg->setToolTip(S);
                return arg->isChecked();
            }
            if constexpr (std::is_same_v<T, ToggleSwitch*>)
            {
                arg->setToolTip(S);
                return arg->isChecked();
            }
        }, m_widget);
    }

    QJsonValue getValue() const override
    {
        return get();
    }

    void setValue(bool val)
    {
        set(val);
    }

    bool get() const
    {
        return
        std::visit([&](auto&& arg)
        {
            using T = std::decay_t<decltype(arg)>;
            if constexpr (std::is_same_v<T, QCheckBox*>)
            {
                return arg->isChecked();
            }
            if constexpr (std::is_same_v<T, ToggleSwitch*>)
            {
                return arg->isChecked();
            }
        }, m_widget);
    }

    void set(bool val)
    {
        std::visit([&](auto&& arg)
        {
            using T = std::decay_t<decltype(arg)>;
            if constexpr (std::is_same_v<T, QCheckBox*>)
            {
                arg->setChecked(val);
            }
            if constexpr (std::is_same_v<T, ToggleSwitch*>)
            {
                arg->setCheckedAnimate(val);
            }
        }, m_widget);
    }

Q_SIGNALS:
    void valueChanged(bool d);
protected:
    Style m_style = Style::UNKNOWN;
    std::variant< QCheckBox*, ToggleSwitch*  > m_widget;

    void _deleteOldWidget()
    {
        std::visit([&](auto&& arg)
        {
            if( arg != nullptr)
            {
                arg->deleteLater();
            }
        }, m_widget);
        m_widget = static_cast<QCheckBox*>(nullptr);
    }
};


/**
 * @brief The QJChoice class
 *
 * Dropdown menu class for enum related properties
 */
class QJChoice : public QWidget
{
    Q_OBJECT
public:

    explicit QJChoice(QWidget* parent) : QWidget(parent)
    {
        auto l = new QHBoxLayout();
        l->setSpacing(0);
        l->setMargin(0);

        m_combo = new QComboBox(this);
        m_combo->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Minimum);

        m_combo->setObjectName("QJChoice::QComboBox");

        l->addWidget(m_combo, 0);

        connect(m_combo, QOverload<int>::of(&QComboBox::currentIndexChanged),
        [this](int i)
        {
             (void)i;
             auto v = this->value();
             emit this->valueChanged( this->value() );
        });

        this->setLayout(l);
    }

    /**
     * @brief value
     * @return
     *
     * Returns the current value.
     */
    QString value() const
    {
        return m_combo->currentData().toString();// currentText();
    }

    /**
     * @brief title
     * @return
     *
     * Returns the title of the currently selected item
     */
    QString title() const
    {
        return m_combo->currentText();
    }

    /**
     * @brief setValue
     * @param S
     *
     * Sets the value to a "string" value in the enum
     */
    void setValue(QString const & S)
    {
        for(int i=0;i<m_combo->count();i++)
        {
            if( m_combo->itemData(i).toString() == S)
            {
                m_combo->setCurrentIndex(i);
                break;
            }
        }
    }

    /**
     * @brief insertItem
     * @param index
     * @param value
     * @param label
     *
     * Insert an item into the
     */
    void insertItem(int index, QString const & value, QString const & label="")
    {
        (void)label;
        m_combo->insertItem(index, label==""?value:label, QVariant(value) );
    }

Q_SIGNALS:
    void valueChanged(QString const &S);

protected:
    QComboBox * m_combo;
};


/**
 * @brief The QJColor class
 *
 * Color picker widget. This is used within the
 * QJString class as an alternative widget.
 */
class QJColor : public QWidget
{
    Q_OBJECT
public:
    explicit QJColor(QWidget* parent) : QWidget(parent)
    {
        auto l = new QHBoxLayout();
        l->setSpacing(0);
        l->setMargin(0);

        m_colorButton = new QToolButton(this);
        m_colorText   = new QLineEdit(this);

        l->addWidget(m_colorText, 3);
        l->addWidget(m_colorButton, 2);


        auto ll = new QHBoxLayout();
        ll->setMargin(5);
        m_colorButton->setLayout(ll);
        auto lb = new QLabel(this);

        ll->addWidget(lb);
        lb->setAutoFillBackground(false);

        m_label = lb;

        m_colorButton->connect( m_colorButton, &QPushButton::clicked,
                           [this](bool x)
        {
            (void)x;
            auto c = QColorDialog::getColor();
            this->setValue(c);
        });

        m_colorText->setAlignment( Qt::AlignCenter);

        m_colorText->connect( m_colorText, &QLineEdit::textChanged, [this](QString const &S)
        {
           if( QColor::isValidColor(S))
           {
               this->setValue( QColor(S));
           }
        });

        this->setLayout(l);
    }

    void setValue(QColor const & C)
    {
        m_color = C;
        QPalette sample_palette;
        sample_palette.setColor(QPalette::Window,  C);

        QString style = "background-color:" + C.name(QColor::HexArgb) + ";";
        m_label->setStyleSheet(style);

        m_label->setAutoFillBackground(true);
        m_label->setPalette(sample_palette);

        m_colorText->setText( C.name(QColor::HexArgb));
    }

    QColor value() const
    {
        return m_color;
    }

    void setValue(QString const & c)
    {
        if( QColor::isValidColor(c))
        {
            setValue( QColor(c));
        }
    }

    QLabel      * m_label;
    QToolButton * m_colorButton;
    QLineEdit   * m_colorText;
    QColor        m_color;
};

class QJPath : public QWidget
{
    Q_OBJECT
public:
    explicit QJPath(QWidget* parent) : QWidget(parent)
    {
        auto l = new QHBoxLayout();
        l->setSpacing(0);
        l->setMargin(0);

        m_colorButton = new QToolButton(this);
        m_colorButton->setText("...");
        m_valueText   = new QLineEdit(this);

        l->addWidget(m_valueText, 3);
        l->addWidget(m_colorButton, 2);


        auto ll = new QHBoxLayout();
        ll->setMargin(5);
        m_colorButton->setLayout(ll);

        m_colorButton->connect( m_colorButton, &QPushButton::clicked,
                           [this](bool x)
        {
            (void)x;
            auto str = m_directory ? QFileDialog::getExistingDirectory(nullptr, "Select a directory") : QFileDialog::getOpenFileName(nullptr, "Select a file");
            this->setValue(str);
        });

        this->setLayout(l);
    }

    void setValue(QString const & C)
    {
        m_valueText->setText( C);
    }

    QString value() const
    {
        return m_valueText->text();
    }

    void setDirectoryOnly(bool f)
    {
        m_directory = f;
    }
    QLabel      * m_label;
    QToolButton * m_colorButton;
    QLineEdit   * m_valueText;
    bool          m_directory=false;
};

/**
 * @brief The QJString class
 *
 * Top level class for string controls.
 */
class QJString : public QJBase
{
    Q_OBJECT
public:
    enum Style
    {
        DEFAULT=0,
        ENUM,
        COLOR,
        FILE,
        DIR,
        UNKNOWN=255
    };

    explicit QJString(QWidget* parent, QJForm* parentForm=nullptr) : QJBase(parent, parentForm)
    {
        auto l = new QHBoxLayout();
        this->setLayout(l);
        l->setSpacing(0);
        l->setMargin(0);
        setStyle(Style::DEFAULT);
    }

    QJString(QJsonObject schema, QWidget* parent, QJForm* parentForm=nullptr) : QJBase(parent, parentForm)
    {
        auto l = new QHBoxLayout();
        this->setLayout(l);
        l->setSpacing(0);
        l->setMargin(0);
        QJString::setSchema(schema);
    }

    Style getStyle() const
    {
        return m_style;
    }

    void setStyle( Style s)
    {
        if( getStyle() == s)
            return;
        _deleteOldWidget();
        m_style = s;
        switch(s)
        {
            case DEFAULT:
            {
                auto cb = new QLineEdit(this);
                cb->setObjectName("QJString::QLineEdit");
                dynamic_cast<QHBoxLayout*>(layout())->addWidget(cb);
                m_widget = cb;

                cb->connect(cb, &QLineEdit::textChanged, [this](QString const&C)
                {
                    emit this->valueChanged(C);
                });
                break;
            }
            case COLOR:
            {
                auto N = new QJColor(this);
                dynamic_cast<QHBoxLayout*>(layout())->addWidget(N);
                m_widget = N;
                N->m_colorText->connect(N->m_colorText, &QLineEdit::textChanged, [this](QString const&C)
                {
                    emit this->valueChanged(C);
                });
                break;
            }
            case FILE:
            {
                auto N = new QJPath(this);
                dynamic_cast<QHBoxLayout*>(layout())->addWidget(N);
                m_widget = N;
                N->m_valueText->connect(N->m_valueText, &QLineEdit::textChanged, [this](QString const&C)
                {
                    emit this->valueChanged(C);
                });
                break;
            }
            case DIR:
            {
                auto N = new QJPath(this);
                N->setDirectoryOnly(true);
                dynamic_cast<QHBoxLayout*>(layout())->addWidget(N);
                m_widget = N;
                N->m_valueText->connect(N->m_valueText, &QLineEdit::textChanged, [this](QString const&C)
                {
                    emit this->valueChanged(C);
                });
                break;
            }
            case ENUM:
            {
                auto N = new QJChoice(this);
                int c = 0;
                for(auto const & X : m_enumsValueLabel)
                {
                    N->insertItem(c++, X.first, X.second);
                }
                dynamic_cast<QHBoxLayout*>(layout())->addWidget(N);
                m_widget = N;
                N->connect(N, &QJChoice::valueChanged, [this](QString const&C)
                {
                    emit this->valueChanged(C);
                });
                break;
            }
            case UNKNOWN:
                throw std::runtime_error("asdasdfs");
            break;
        }
    }

    void setSchema(const QJsonObject & J) override
    {
        auto mI = J.find("ui:widget");
        QString wid="default";
        if( mI != J.end() )
        {
            wid = mI->toString();
        }

        {
            QString desc = "";
            auto idesc = J.find("description");
            if( idesc != J.end())
                desc = idesc->toString();
            this->setDescription(desc);
        }

        auto i = J.find("enum");
        auto j = J.find("enumNames");
        if( i != J.end() )
        {
            auto enumValues = i->toArray();
            auto enumLabels = j==J.end() ? enumValues : j->toArray();

            for(int k=0;k<enumValues.size();k++)
            {
                m_enumsValueLabel.push_back( {enumValues.at(k).toString() , enumLabels.at(k).toString()} );
            }

            setStyle(Style::ENUM);
        }
        else
        {
            if( wid == "default")
            {
                setStyle(Style::DEFAULT);
            }
            else if( wid == "color")
            {
                setStyle(Style::COLOR);
            }
            else if( wid == "file")
            {
                setStyle(Style::FILE);
            }
            else if( wid == "dir")
            {
                setStyle(Style::DIR);
            }
        }

        auto md = J.find("default");
        if( md != J.end())
        {
            set( md->toString());
        }
    }

    void setValue( QJsonValue const & v) override
    {
        if( !v.isString() )
            return;

        std::visit([&](auto&& arg)
        {
            using T = std::decay_t<decltype(arg)>;
            if constexpr (std::is_same_v<T, QLineEdit*>)
            {
                arg->setText( v.toString() );
            }
            if constexpr (std::is_same_v<T, QJChoice*>)
            {
                arg->setValue( v.toString() );
            }
            if constexpr (std::is_same_v<T, QJColor*>)
            {
                arg->setValue( v.toString() );
            }
            if constexpr (std::is_same_v<T, QJPath*>)
            {
               arg->setValue( v.toString() );
            }
        }, m_widget);
    }

    void setDescription(QString const & S) override
    {
        (void)S;
        std::visit([&](auto&& arg)
        {
            (void)arg;
            //using T = std::decay_t<decltype(arg)>;
          //  arg->setToolTip(S);
        }, m_widget);
    }

    QJsonValue getValue() const override
    {
        return get();
    }
    void setValue(QString const & val)
    {
        set(val);
    }

    QString get() const
    {
        return
        std::visit([&](auto&& arg) -> QString
        {
            using T = std::decay_t<decltype(arg)>;
            if constexpr (std::is_same_v<T, QLineEdit*>)
            {
                return arg->text();
            }
            else if constexpr (std::is_same_v<T, QJChoice*>)
            {
                return arg->value();
            }
            else if constexpr (std::is_same_v<T, QJColor*>)
            {
                return arg->value().name();
            }
            else if constexpr (std::is_same_v<T, QJPath*>)
            {
                return arg->value();
            }
            return "";
        }, m_widget);
    }

    void set(QString const & val)
    {
        std::visit([&](auto&& arg)
        {
            using T = std::decay_t<decltype(arg)>;
            if constexpr (std::is_same_v<T, QLineEdit*>)
            {
                arg->setText(val);
            }
            if constexpr (std::is_same_v<T, QJColor*>)
            {
                arg->setValue(val);
            }
            if constexpr (std::is_same_v<T, QJPath*>)
            {
                arg->setValue(val);
            }
        }, m_widget);
    }

Q_SIGNALS:

    void valueChanged(QString const &s);

protected:
    Style m_style = Style::UNKNOWN;
    std::vector< std::pair<QString,QString> > m_enumsValueLabel;
    std::variant< QLineEdit*, QJChoice*, QJColor* , QJPath*> m_widget;
    void _deleteOldWidget()
    {
        std::visit([&](auto&& arg)
        {
            if( arg != nullptr)
            {
                arg->deleteLater();
            }
        }, m_widget);
        m_widget = static_cast<QLineEdit*>(nullptr);
    }
};


/**
 * @brief The QJSlider class
 *
 * Slider class for producing range numbers.
 */
class QJSlider : public QWidget
{
    Q_OBJECT
public:
    explicit QJSlider(QWidget* parent) : QWidget(parent)
    {
        auto l = new QHBoxLayout();
        l->setSpacing(0);
        l->setMargin(0);

        m_slider = new QSlider(this);
        m_slider->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Minimum);
        m_text = new QLineEdit(this);

        m_slider->setObjectName("QJSlider::QSlider");
        m_slider->setMinimum( 0 );
        m_slider->setMaximum( std::numeric_limits<int>::max() );
        m_text->setObjectName("QJSlider::QLineEdit");

        m_slider->setOrientation( Qt::Horizontal);

        l->addWidget(m_slider, 3);
        l->addWidget(m_text, 1);

        m_slider->connect( m_slider, &QSlider::valueChanged,
                           [this](int x)
        {
            (void)x;
            auto v = this->value();
            m_text->setText( QString( std::to_string(v).c_str()));
        });

        m_text->setReadOnly(true);
        this->setLayout(l);
    }

    double value() const
    {
        auto v = static_cast<double>(m_slider->value());
        auto m = static_cast<double>(m_slider->minimum());
        auto M = static_cast<double>(m_slider->maximum());
        auto s = (v-m) / (M-m);

        v = (1-s)*m_min + s * m_max;
        return v;
    }

    void setValue(double v)
    {
        auto m = static_cast<double>(m_slider->minimum());
        auto M = static_cast<double>(m_slider->maximum());

        v = std::clamp(v, m_min, m_max);

        auto s = (v-m_min) / (m_max-m_min);

        v = (1-s)*m + s * M;

        m_slider->setValue( static_cast<int>(v) );
    }

    void setMinimum(double m)
    {
        auto cv = value();
        m_min = m;
        setValue(cv);
    }
    void setMaximum(double m)
    {
        auto cv = value();
        m_max = m;
        setValue(cv);
    }

    double m_min=0;
    double m_max=100;
    QSlider * m_slider;
    QLineEdit * m_text;
};

/**
 * @brief The QJNumber class
 *
 * Top level class for number controls. The chosen widget is
 * determined by the ui:widget property in the json object.
 *
 * "ui:widget" : "range" -> slider
 */
class QJNumber : public QJBase
{
    Q_OBJECT
public:
    enum Style
    {
        DEFAULT=0,
        RANGE=1,
        UNKNOWN=255
    };

    explicit QJNumber(QWidget* parent, QJForm* parentForm=nullptr) : QJBase(parent, parentForm)
    {
        auto l = new QHBoxLayout();
        this->setLayout(l);
        l->setSpacing(0);
        l->setMargin(0);
        setStyle(Style::DEFAULT);
    }

    QJNumber(QJsonObject schema, QWidget* parent, QJForm* parentForm=nullptr) : QJBase(parent, parentForm)
    {
        auto l = new QHBoxLayout();
        l->setSpacing(0);
        l->setMargin(0);
        this->setLayout(l);
        QJNumber::setSchema(schema);
    }

    Style getStyle() const
    {
        return m_style;
    }

    void setStyle( Style s)
    {
        if( getStyle() == s)
            return;
        _deleteOldWidget();
        m_style = s;
        switch(s)
        {
            case DEFAULT:
            {
                auto cb = new QLineEdit(this);
                cb->setObjectName("QJNumber::QLineEdit");
                cb->setValidator(new QRegExpValidator(QRegExp("\\-?[0-9\\.]*"), cb));
                dynamic_cast<QHBoxLayout*>(layout())->addWidget(cb);
                m_widget = cb;

                cb->connect(cb, &QLineEdit::textChanged, [this](QString const &C)
                {
                    (void)C;
                    emit this->valueChanged(this->get());
                });
                break;
            }
            case RANGE:
            {
                auto cb = new QJSlider(this);
                cb->setObjectName("QJNumber::QJSlider");
                dynamic_cast<QHBoxLayout*>(layout())->addWidget(cb);
                m_widget = cb;
                cb->m_slider->connect(cb->m_slider, &QSlider::valueChanged, [this](int C)
                {
                    (void)C;
                    emit this->valueChanged(this->get());
                });
                break;
            }

            case UNKNOWN:
            default:
                throw std::runtime_error("Invalid style");
            break;
        }
    }

    void setSchema(const QJsonObject & J) override
    {
        auto mI = J.find("ui:widget");
        QString wid="default";
        if( mI != J.end() )
        {
            wid = mI->toString();
        }



        if( wid == "default")
        {
            setStyle(Style::DEFAULT);
        }
        else if( wid == "range")
        {
            setStyle(Style::RANGE);
        }

        auto md = J.find("default");
        if( md != J.end())
        {
            set( md->toDouble() );
        }
        {
            QString desc = "";
            auto idesc = J.find("description");
            if( idesc != J.end())
                desc = idesc->toString();
            this->setDescription(desc);
        }
    }

    void setValue( QJsonValue const & v) override
    {
        if( !v.isDouble() )
            return;
        set( v.toDouble());
    }

    QJsonValue getValue() const override
    {
        return get();
    }

    void setDescription(QString const & S) override
    {
        std::visit([&](auto&& arg)
        {
            //using T = std::decay_t<decltype(arg)>;
            arg->setToolTip(S);
        }, m_widget);
    }

    void setValue(double val)
    {
        set(val);
    }

    double get() const
    {
        return
        std::visit([&](auto&& arg)
        {
            using T = std::decay_t<decltype(arg)>;
            if constexpr (std::is_same_v<T, QDoubleSpinBox*>)
            {
                return arg->value();
            }
            if constexpr (std::is_same_v<T, QLineEdit*>)
            {
                return arg->text().toDouble();
            }
            if constexpr (std::is_same_v<T, QJSlider*>)
            {
                return static_cast<double>(arg->value());
            }
        }, m_widget);
    }

    void set(double val)
    {
        std::visit([&](auto&& arg)
        {
            using T = std::decay_t<decltype(arg)>;
            if constexpr (std::is_same_v<T, QLineEdit*>)
            {
                arg->setText( QString( std::to_string(val).c_str()));
            }
            if constexpr (std::is_same_v<T, QDoubleSpinBox*>)
            {
                arg->setValue(val);
            }
            if constexpr (std::is_same_v<T, QJSlider*>)
            {
                arg->setValue(val);
            }
        }, m_widget);
    }

Q_SIGNALS:
    void valueChanged(double d);

protected:
    Style m_style = Style::UNKNOWN;
    std::variant< QLineEdit*, QDoubleSpinBox*, QJSlider* > m_widget;

    void _deleteOldWidget()
    {
        std::visit([&](auto&& arg)
        {
            if( arg != nullptr)
            {
                arg->deleteLater();
            }
        }, m_widget);
        m_widget = static_cast<QLineEdit*>(nullptr);
    }
};


/**
 * @brief The QJObject class
 *
 * Container Widget class to display JSON properties.
 */
class QJObject : public QJBase
{
    Q_OBJECT
public:
    std::vector< QJsonObject > m_oneOf;
    QFormLayout * m_propertyLayout = nullptr;
    QComboBox   * m_oneOfCombo = nullptr;

    QJBase* getRootParent();

    explicit QJObject(QWidget* parent, QJForm* parentForm=nullptr) : QJBase(parent, parentForm)
    {
        _buildUI();
    }

    QJObject(QJsonObject schema, QWidget* parent, QJForm* parentForm=nullptr) : QJBase(parent, parentForm)
    {
        _buildUI();
        QJObject::setSchema(schema);
    }

    /**
     * @brief getOneOfCount
     * @return
     *
     * Return the number of oneOf options
     */
    int getOneOfCount() const
    {
        return static_cast<int>(m_oneOf.size());
    }

    /**
     * @brief addOneOf
     * @param B
     * Add a oneOf option.
     */
    void addOneOf(QJsonObject const & schema)
    {
        m_oneOf.push_back(schema);
        QString label = "Object_0";
        auto it = schema.find("title");
        if( it != schema.end())
            label = it->toString();
        m_oneOfCombo->addItem(label);
        m_oneOfCombo->setVisible( getOneOfCount() > 1 );
    }

    /**
     * @brief removeOneOf
     * @param index
     *
     * Remove a oneOf option
     */
    void removeOneOf(int index)
    {
        index = std::clamp( index, 0, getOneOfCount()-1);
        m_oneOf.erase(m_oneOf.begin()+index);
        m_oneOfCombo->removeItem(index);

        m_oneOfCombo->setVisible( getOneOfCount() > 1 );
    }

    /**
     * @brief findItem
     * @param property
     * @return
     *
     * Gets the item from from the object that matches the property name.
     */
    QJBase* getItem(QString const & property) const
    {
        auto L = m_propertyLayout;
        auto s = L->rowCount();

        QJsonObject ar;
        for(int i=0; i<s;i++)
        {
            auto w = L->itemAt(i, QFormLayout::FieldRole)->widget();
            auto b = getQJBase(w);
            if( b)
            {
                if ( b->getPropertyLabel() == property)
                {
                    return b;
                }
            }
        }
        return nullptr;
    }

    /**
     * @brief getItemCount
     * @return
     *
     * Returns the total number of properties in the object
     */
    int getItemCount() const
    {
        return m_propertyLayout->rowCount();
    }

    /**
     * @brief findChildRecursively
     * @param path
     * @return
     *
     * Finds a child based on a json schema path
     * eg:
     *
     * {
     *      "user" : [
     *          {
     *              "firstName" : "Clark",
     *              "lastName" : "Kent"
     *          },
     *          {
     *              "firstName" : "Bruce",
     *              "lastName" : "Wayne"
     *          }
     *      ]
     * }
     *
     * findChildRecusrively( "user/0/lastName" ) will return the
     * widget that holds the value "Kent"
     */
    QJBase const * findChildRecursively(QString const & path) const;
    QJBase  * findChildRecursively(QString const & path);

    /**
     * @brief takeItem
     * @param row
     * @return
     *
     * Takes the item out of the layout and returns the widget.
     * You are now responsible for deleting the widget.
     */
    [[ nodiscard ]] QWidget* takeItem(int row)
    {
        row = std::clamp(row, 0, m_propertyLayout->rowCount()-1);
        auto R = m_propertyLayout->takeRow(row);

        if( R.labelItem )
        {
            if( R.labelItem->widget() )
               delete R.labelItem->widget();
            delete R.labelItem;
        }
        return R.fieldItem->widget();
    }

    /**
     * @brief takeProperty
     * @param row
     * @return
     *
     * Takes the widget out of the layout and returns the widget.
     * You are now responsible for deleting the widget.
     */
    [[ nodiscard ]] QJBase* takeProperty(QString const & property)
    {
        auto L = m_propertyLayout;
        auto s = L->rowCount();

        QJsonObject ar;
        for(int i=0; i<s;i++)
        {
            auto w = L->itemAt(i, QFormLayout::FieldRole)->widget();

            auto wB = getQJBase(w);
            if( wB)
            {
                if(wB->getPropertyLabel() == property)
                {
                    return takeQJBase(wB);
                }
            }
            continue;
            if( auto jw = dynamic_cast<QJBase*>(w) )
            {
                assert(wB == jw);
                if(jw->getPropertyLabel() == property)
                {
                    return dynamic_cast<QJBase*>( takeItem(i));
                }
            }
            else if( auto jw1 = dynamic_cast<CollapsableWidget*>(w) )
            {
                auto cent = dynamic_cast<QJObject*>( jw1->widget() );
                if(cent->getPropertyLabel() == property)
                {
                    auto x = dynamic_cast<CollapsableWidget*>(takeItem(i));
                    assert(x==jw1);

                    auto p = dynamic_cast<QJBase*>(jw1->takeWidget());
                    assert(wB == p);
                    x->deleteLater();
                    return p;
                }
            }
        }
        return nullptr;
    }


    virtual void setSchema(const QJsonObject & J) override;

    /**
     * @brief addProperty
     * @param prop - the label to give the object
     * @param v - the actual widget
     * @param title - the title
     * @param showLabel - show the label/title?
     *
     * Add a property to the layout, if showLabel is false, it will
     * add it as a full row item
     */
    void addProperty(QString const & prop, QJBase * v, QString const & title="", bool showLabel=true)
    {
        v->setProperty(prop);
        v->setParent(this);
        addPropertyWidget(prop, v, title, showLabel);
    }

    QJsonValue getValue() const override
    {
        auto L = m_propertyLayout;
        auto s = L->rowCount();

        QJsonObject ar;
        for(int i=0; i<s;i++)
        {
            auto w = L->itemAt(i, QFormLayout::FieldRole)->widget();

            auto jw = dynamic_cast<QJBase*>(w);
            if(jw)
            {
                ar[jw->getPropertyLabel()]  = jw->getValue();
            }
            else
            {
                auto jw2 = dynamic_cast<CollapsableWidget*>(w);
                if( jw2 )
                {
                    jw = dynamic_cast<QJBase*>(jw2->widget());
                    ar[jw->getPropertyLabel()]  = jw->getValue();
                }
            }
        }
        return ar;
    }

    void setSpacing(int i)
    {
        m_propertyLayout->setSpacing(i);
        m_propertyLayout->setVerticalSpacing(i);
    }

    void setMargin(int i)
    {
        m_propertyLayout->setMargin(i);
    }
Q_SIGNALS:
    void valueChanged(QString const& path);
    void widgetValueChanged(QJBase * widget);

protected:

    void addPropertyWidget(QString const & prop, QWidget * v, QString const & title="", bool showLabel=true)
    {
        auto label = title=="" ? prop : title;

        if( showLabel)
        {
            m_propertyLayout->addRow(label, v);
        }
        else
        {
            m_propertyLayout->addRow(v);
        }
    }

    void setOneOf(QJsonObject const & obj);

    void _buildUI()
    {
        auto l = new QFormLayout(this);

        m_propertyLayout = new QFormLayout();

        setMargin(0);
        setSpacing(2);

        m_propertyLayout->setVerticalSpacing(2);
        m_oneOfCombo = new QComboBox();
        m_oneOfCombo->setVisible(false);
        l->addRow(m_oneOfCombo);
        l->addRow(m_propertyLayout);


        m_oneOfCombo->connect( m_oneOfCombo, QOverload<int>::of(&QComboBox::currentIndexChanged),
                               [this](int index)
        {
            index = std::clamp(index, 0, getOneOfCount()-1);
            this->setOneOf(this->m_oneOf.at( static_cast<size_t>(index) ));
        });
    }

    std::pair<QJBase*, QLabel*> getItemLabel(QString const & property)
    {
        auto L = m_propertyLayout;
        auto s = L->rowCount();

        QJsonObject ar;

        for(int i=0; i<s ; i++)
        {
            auto w = L->itemAt(i, QFormLayout::FieldRole)->widget();
            auto l = static_cast<QLabel*>( L->itemAt(i, QFormLayout::LabelRole)->widget() );

            if( auto jw = dynamic_cast<QJBase*>(w) )
            {
                if(jw->getPropertyLabel() == property)
                {
                    return std::make_pair(jw,l);
                }
            }
            else if( auto jw1 = dynamic_cast<CollapsableWidget*>(w) )
            {
                auto cent = dynamic_cast<QJObject*>( jw1->widget() );
                if(cent->getPropertyLabel() == property)
                {
                    return std::make_pair(cent,nullptr);
                }
            }
        }
        return {nullptr,nullptr};
    }

public:
    void setValue(const QJsonValue &V) override
    {
        auto p = V.toObject();

        for(auto i=p.begin(); i!=p.end();i++)
        {
            auto xx = getItemLabel(i.key());
            if( xx.first )
            {
                xx.first->setValue(i.value());
            }
        }
    }
};




/**
 * @brief The QJArrayItem class
 *
 * Class to handle items within an array.
 *  This is a container-style class which provides
 *  up/down/delete items for a widget.
 */
class QJArrayItem : public QWidget
{
    Q_OBJECT
public:
    explicit QJArrayItem(QWidget* centralWidget, QWidget* parent=nullptr) : QWidget(parent)
    {
        m_buttons = new QWidget(this);
        auto hb = new QHBoxLayout();
        m_buttons->setLayout( hb );
        hb->setSpacing(1);
        auto l = new QHBoxLayout();
        l->setSpacing(1);
        l->setMargin(0);

        hb->setMargin(0);

        m_up      = new QToolButton(this);
        m_up->setArrowType(Qt::UpArrow);

        m_down    = new QToolButton(this);
        m_down->setArrowType(Qt::DownArrow);

        m_delete  = new QToolButton(this);
        m_delete->setText("✖");

        m_up->setMaximumSize( 25,25);
        m_down->setMaximumSize( 25,25);
        m_delete->setMaximumSize( 25,25);

        m_buttons->layout()->addWidget(m_up);
        m_buttons->layout()->addWidget(m_down);
        m_buttons->layout()->addWidget(m_delete);

        l->addWidget(centralWidget,1);
        l->addWidget(m_buttons,0);
        l->setSpacing(5);
        l->setMargin(0);
        this->setLayout(l);

        m_buttons->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Minimum);
        m_centralWidget = centralWidget;

        m_up->connect( m_down, &QToolButton::pressed, [this]()
        {
            emit this->indexChanged(1);
        });
        m_down->connect( m_up, &QToolButton::pressed, [this]()
        {
            emit this->indexChanged(-1);
        });
        m_delete->connect( m_delete, &QToolButton::pressed, [this]()
        {
            emit this->indexChanged(0);
        });
    }

    QWidget* centralWidget()
    {
        return m_centralWidget;
    }
    [[ nodiscard ]] QWidget* takeCentralWidget()
    {
        return layout()->takeAt(0)->widget();
    }
    void setCentralWidget(QWidget* w)
    {
        dynamic_cast<QHBoxLayout*>(layout())->insertWidget(0, w);
    }

    void enableDeleteButton(bool d)
    {
        m_delete->setVisible(d);
    }
    void enableArrowButtons(bool d)
    {
        m_up->setVisible(d);
        m_down->setVisible(d);
    }
    void enableButtons(bool d)
    {
        m_buttons->setVisible(d);
    }
Q_SIGNALS:
    void indexChanged(int rel);

protected:
    QWidget     * m_centralWidget;
    QWidget     * m_buttons;
    QToolButton * m_up;
    QToolButton * m_down;
    QToolButton * m_delete;
};


class QJArray : public QJBase
{
    Q_OBJECT
    std::vector<QJsonObject> m_additionalItemsOneOf;
    QFormLayout * m_itemLayout = nullptr;
    QFormLayout * m_widgetLayout = nullptr;
    QPushButton * m_addItem = nullptr;
    QComboBox   * m_selectItem = nullptr;
public:

    explicit QJArray(QWidget* parent, QJForm* parentForm=nullptr) : QJBase(parent, parentForm)
    {
        _buildUI();
        setMargin(0);
        setSpacing(2);
    }

    QJArray(QJsonObject schema, QWidget* parent, QJForm* parentForm=nullptr) : QJBase(parent, parentForm)
    {
        _buildUI();
        QJArray::setSchema(schema);
        setMargin(0);
        setSpacing(2);
    }

    void _buildUI()
    {
        m_itemLayout   = new QFormLayout();
        m_widgetLayout = new QFormLayout(this);
        m_widgetLayout->addRow(m_itemLayout);

        auto buttonWidget = new QWidget(this);
        buttonWidget->setLayout( new QHBoxLayout() );
        buttonWidget->layout()->setSpacing(0);
        buttonWidget->layout()->setMargin(0);

        auto btnAddItem = new QPushButton(this);
        btnAddItem->setText("✚");
        btnAddItem->setMaximumWidth(75);

        auto cbmItems = new QComboBox(this);

        buttonWidget->layout()->addWidget(new QWidget());
        buttonWidget->layout()->addWidget(cbmItems);
        buttonWidget->layout()->addWidget(btnAddItem);

        m_widgetLayout->addWidget( buttonWidget );

        btnAddItem->connect(btnAddItem, &QPushButton::clicked,
                        [this, cbmItems](bool checked)
        {
            (void)checked;
            auto i = cbmItems->currentIndex();
            if( i < 0)
                return;
            auto N = createItem(m_additionalItemsOneOf.at( static_cast<size_t>(i) ));
            push_back(N);
        });

        m_selectItem = cbmItems;
        m_addItem    = btnAddItem;

        m_selectItem->setVisible(false);
        m_addItem->setVisible(false);
    }

    /**
     * @brief getRootParent
     * @return
     *
     * Returns the top most parent. That is the ancestor that
     * does not have a parent.
     */
    QJBase *getRootParent()
    {
        auto p = getParent();
        if( auto o = dynamic_cast<QJObject*>(p))
        {
            if( o->getParent() == nullptr)
                return o;

            return o->getRootParent();
        }
        else if( auto a = dynamic_cast<QJArray*>(p))
        {
            if( a->getParent() == nullptr)
                return a;

            return a->getRootParent();
        }
        return this;
    }

    /**
     * @brief getItem
     * @param index
     * @return
     *
     * Returns the item at a paricular version. index must be
     * a string version of a number...eg "1", "2"
     */
    QJBase* getItem(QString const &index) const
    {
        return getItem( index.toInt());
    }

    /**
     * @brief getItem
     * @param index
     * @return
     *
     * Returns an item at a paricular index. returns nullptr if
     * it doesnt exist.
     */
    QJBase* getItem(int index) const
    {
        auto L = m_itemLayout;
        auto c = itemCount();//L->rowCount();
        if( index >= c)
            return nullptr;


        {
            auto w = L->itemAt(index, QFormLayout::SpanningRole)->widget();
            auto jw = dynamic_cast<QJArrayItem*>(w);
            if(jw)
            {
                auto jb = dynamic_cast<QJBase*>(jw->centralWidget());
                if( jb )
                {
                    return jb;
                }
            }
        }
        return nullptr;
    }

    /**
     * @brief findChildRecursively
     * @param path
     * @return
     *
     * Finds a child based on a json schema path
     * eg:
     *
     * {
     *      "user" : [
     *          {
     *              "firstName" : "Clark",
     *              "lastName" : "Kent"
     *          },
     *          {
     *              "firstName" : "Bruce",
     *              "lastName" : "Wayne"
     *          }
     *      ]
     * }
     *
     * findChildRecusrively( "user/0/lastName" ) will return the
     * widget that holds the value "Kent"
     */
    QJBase const * findChildRecursively(QString const & path) const;
    QJBase  * findChildRecursively(QString const & path);

    /**
     * @brief itemAt
     * @param i
     * @return
     *
     * Returns an item at a paricular index. Returns nullptr if out of bounds
     */
    QJBase* itemAt(int i)
    {
        if( auto jw = ArrayitemAt(i))
        {
            return dynamic_cast<QJBase*>(jw->centralWidget());
        }
        return nullptr;
    }

    QJArrayItem* ArrayitemAt(int i)
    {
        auto L = m_itemLayout;
        auto s = itemCount();
        if( i < s)
        {
            auto w = L->itemAt(i, QFormLayout::SpanningRole)->widget();
            auto jw = dynamic_cast<QJArrayItem*>(w);
            if(jw)
            {
                return jw;
            }
        }
        return nullptr;
    }

    /**
     * @brief getAdditionalItemCount
     * @return
     *
     * Returns the total number of additional item types.
     */
    size_t getAdditionalItemCount() const
    {
        return m_additionalItemsOneOf.size();
    }

    /**
     * @brief addAdditionalItem
     * @param obj
     *
     * Adds an additional item that the user can select.
     * Additional items allow the user to add new entries
     * to the array.
     */
    void addAdditionalItem(QJsonObject const & obj)
    {
        m_additionalItemsOneOf.push_back(obj);
        QString label = QString("Item_0");// + QString( m_additionalItemsOneOf.size() );
        auto it = obj.find("title");
        if( it != obj.end())
            label = it->toString();
        m_selectItem->addItem(label);

        m_selectItem->setVisible( m_additionalItemsOneOf.size() > 1 );
        m_addItem->setVisible(    m_additionalItemsOneOf.size() > 0 );
    }

    /**
     * @brief removeAdditionalItem
     * @param index
     *
     * Removes an additional item option.
     * If index < 0 it will remove the first option.
     * if index >= getAdditinalItemCount() it will remove the last option
     */
    void removeAdditionalItem(int index)
    {
        index = std::clamp(index, 0, static_cast<int>(getAdditionalItemCount()-1) );

        m_selectItem->removeItem(index);
        m_additionalItemsOneOf.erase( m_additionalItemsOneOf.begin()+index);

        m_selectItem->setVisible( m_additionalItemsOneOf.size() > 1 );
        m_addItem->setVisible(    m_additionalItemsOneOf.size() > 0 );
    }

    /**
     * @brief setSchema
     * @param J
     *
     * Sets the schema of the widget.
     */
    virtual void setSchema(const QJsonObject & J) override
    {
        {
            auto mI = J.find("additionalItems");
            if( mI != J.end() )
            {
                if( mI->isObject())
                {
                    auto additionalItem = mI->toObject();
                    auto it = additionalItem.find("oneOf");
                    if( it != additionalItem.end() )
                    {
                        if( it->isArray() )
                        {
                            auto arr = it->toArray();
                            for(auto i : arr)
                            {
                                addAdditionalItem(i.toObject());
                            }
                        }
                        else
                        {
                            throw std::runtime_error("The additionalItems/oneOf property must be an array");
                        }
                    }
                    else
                    {
                        addAdditionalItem( additionalItem );
                    }
                }
                else
                {
                    throw std::runtime_error("The additionalItems must be an Object");
                }
            }
        }

        auto it = J.find("items");
        if( it->isArray())
        {
            auto p = it->toArray();

            for(auto i=p.begin(); i!=p.end();i++)
            {
                auto vO = i->toObject();
                push_back(vO);
            }
        }
    }


    /**
     * @brief itemCount
     * @return
     *
     *
     * Returns the number of items in this array
     */
    int itemCount() const
    {
        auto L = m_itemLayout;
        return L->rowCount();
    }

    /**
     * @brief takeItemAt
     * @param i
     * @return
     *
     * Take the item at index i. Calling this function
     * removes the widget from the array and does not
     * manage it anymore. You must manage the returned
     * widget
     */
    [[ nodiscard ]] QJBase * takeItemAt(int i)
    {
        auto L = m_itemLayout;
        if( itemCount() == 0)
            return nullptr;

        i = std::clamp(i, 0, itemCount() - 1);

        auto lI = L->takeRow(i);

        assert( lI.fieldItem != nullptr);

        auto w = lI.fieldItem->widget();

        auto b2 = takeQJBase(w);
        if( w != b2)
        {
            w->deleteLater();
        }
        return b2;

    }

    /**
     * @brief push_back
     * @param v
     *
     * Push an item to the end of the array.
     * The pushed widget will now be managed by
     * the QJArray
     */
    void push_back(QWidget * v)
    {
        auto x = new QJArrayItem(v, this);

        x->enableArrowButtons(false);
        x->enableDeleteButton(false);
        x->enableButtons(false);
        if( m_additionalItemsOneOf.size() )
        {
            x->enableButtons(true);
            //x->enableArrowButtons(false);
            x->enableDeleteButton(true);
        }
//        v->setParent(x);

        int rowCount = itemCount();

        getQJBase(v)->setParent(this);
        getQJBase(v)->setProperty( QString( std::to_string(rowCount).c_str() ));

        m_itemLayout->insertRow(rowCount, x );

        // lambda slot to remove/reorder the items.
        x->connect(x, &QJArrayItem::indexChanged,[x, this](int d)
        {
            int rowPtr;
            QFormLayout::ItemRole rolePtr;

            m_itemLayout->getWidgetPosition(x, &rowPtr, &rolePtr);
            m_itemLayout->takeRow(x);

            if( d==0 )
            {
                x->deleteLater();
            } else {
                dynamic_cast<QFormLayout*>(layout())->insertRow(rowPtr+d, x);
            }
        });
    }

    /**
     * @brief push_back
     * @param itemSchema
     *
     * Push a new object to the end of the array.
     */
    void push_back( QJsonObject const & itemSchema)
    {
        auto N = createItem(itemSchema);
        if( N )
        {
            push_back(N);
        }
    }

    /**
     * @brief pop_back
     * Remove the last object from the array and deletes it.
     */
    void pop_back()
    {
        auto d = takeItemAt( itemCount()-1);
        if(d)
        {
            d->deleteLater();
        }
    }

    /**
     * @brief getValue
     * @return
     *
     * Gets the value of the fields in this widget as a json array
     *
     */
    QJsonValue getValue() const override
    {
        auto L = m_itemLayout;
        auto s = L->count();

        QJsonArray ar;
        for(int i=0; i<s;i++)
        {
            auto w = L->itemAt(i, QFormLayout::SpanningRole)->widget();
            auto jw = dynamic_cast<QJArrayItem*>(w);
            if(jw)
            {
                auto jb = dynamic_cast<QJBase*>(jw->centralWidget());
                if( jb )
                {
                    ar.push_back( jb->getValue() );
                }
            }
        }
        return ar;
    }


    //=========================================
    void setSpacing(int i)
    {
        auto l = m_itemLayout;
        l->setSpacing(i);
        l->setVerticalSpacing(i);
    }
    void setMargin(int i)
    {
        auto l = m_itemLayout;
        l->setMargin(i);
    }
protected:
    std::variant< QLineEdit*, QDoubleSpinBox*, QJSlider* > m_widget;

    void _deleteOldWidget()
    {
        std::visit([&](auto&& arg)
        {
            if( arg != nullptr)
            {
                arg->deleteLater();
            }
        }, m_widget);
        m_widget = static_cast<QLineEdit*>(nullptr);
    }

    /**
     * @brief createItem
     * @param schema
     * @return
     *
     * Create an QJBase item using a JsonSchema
     */
    [[ nodiscard ]] QWidget* createItem(QJsonObject const & schema)
    {
        auto type = schema.find("type")->toString();
        if( type == "number")
        {
            auto N = new QJNumber(schema,this, getParentForm());
            N->connect(N, &QJNumber::valueChanged, [this, N](double d)
            {
                (void)d;
                auto p = this->getRootParent();
                if( auto o = dynamic_cast<QJObject*>(p))
                {
                    emit o->widgetValueChanged(N);
                    emit o->valueChanged( N->getPath() );
                }
            });
            return N;
        }
        else if (type == "string")
        {
            auto N = new QJString(schema, this, getParentForm());
            N->connect(N, &QJString::valueChanged, [this, N](QString const & d)
            {
                (void)d;
                auto p = this->getRootParent();
                if( auto o = dynamic_cast<QJObject*>(p))
                {
                    emit o->widgetValueChanged(N);
                    emit o->valueChanged( N->getPath() );
                }
            });
            return N;
        }
        else if (type == "boolean")
        {
            auto N = new QJBoolean(schema,this, getParentForm());
            N->connect(N, &QJBoolean::valueChanged, [this, N](bool d)
            {
                (void)d;
                auto p = this->getRootParent();
                if( auto o = dynamic_cast<QJObject*>(p))
                {
                    emit o->widgetValueChanged(N);
                    emit o->valueChanged( N->getPath() );
                }
            });
            return N;
        }
        else if( type == "array")
        {
            auto N = new QJArray(schema,this, getParentForm());
            return N;
        }
        else if( type == "object")
        {
            auto N = new QJObject(schema,this, getParentForm());

            QString ui_widget = "default";
            auto it = schema.find("ui:widget");
            if( it->isString() )
                ui_widget = it->toString();

            if( ui_widget == "window")
            {
                auto c = new CollapsableWidget(this);
                auto willbeNull = c->setWidget(N);
                (void)willbeNull;

                //c->setText(title);
                return c;
            }
            else if( ui_widget == "group")
            {
                auto c = new QGroupBox(this);

                auto L = new QVBoxLayout();
                L->setSpacing(1);
                L->setMargin(0);

                c->setLayout( L );
                c->layout()->addWidget(N);

                return c;
            }
            else
            {
                return N;
            }

            return N;
        }
        return nullptr;
    }



    // QJBase interface
public:
    void setValue(QJsonValue const & value) override;
};

inline QJBase *QJObject::getRootParent()
{
    auto p = getParent();
    if( auto o = dynamic_cast<QJObject*>(p))
    {
        if( o->getParent() == nullptr)
            return o;

        return o->getRootParent();
    }
    else if( auto a = dynamic_cast<QJArray*>(p))
    {
        if( a->getParent() == nullptr)
            return a;

        return a->getRootParent();
    }
    return this;
}

inline QJBase const * QJObject::findChildRecursively(const QString &path) const
{
    auto parts = path.split("/");//, QString::SkipEmptyParts);
    QJBase const * curr = this;

    for(int i=0;i<parts.size();i++)
    {
        auto & p = parts[i];
        if( p == "") continue;
        if( auto * arr = dynamic_cast<QJObject const*>(curr))
        {
            curr = arr->getItem(p);
        }
        else if ( auto * arr1 = dynamic_cast<QJArray const*>(curr) )
        {
            curr = arr1->getItem(p);
        }
        else
        {
            return nullptr;
        }
    }
    return curr;
}

inline QJBase * QJObject::findChildRecursively(const QString &path)
{
    auto parts = path.split("/");//, QString::SkipEmptyParts);
    QJBase * curr = this;

    for(int i=0;i<parts.size();i++)
    {
        auto & p = parts[i];
        if( p == "") continue;
        if( auto * arr = dynamic_cast<QJObject* >(curr))
        {
            curr = arr->getItem(p);
        }
        else if ( auto * arr1 = dynamic_cast<QJArray*>(curr) )
        {
            curr = arr1->getItem(p);
        }
        else
        {
            return nullptr;
        }
    }
    return curr;
}


inline QJBase * QJArray::findChildRecursively(const QString &path)
{
    auto parts = path.split("/");//, QString::SkipEmptyParts);
    QJBase * curr = this;

    for(int i=0;i<parts.size();i++)
    {
        auto & p = parts[i];
        if( p=="") continue;
        if( auto * arr = dynamic_cast<QJObject* >(curr))
        {
            curr = arr->getItem(p);
        }
        else if ( auto * arr1 = dynamic_cast<QJArray*>(curr) )
        {
            curr = arr1->getItem(p);
        }
        else
        {
            return nullptr;
        }
    }
    return curr;
}


inline QJBase const * QJArray::findChildRecursively(const QString &path) const
{
    auto parts = path.split("/");//, QString::SkipEmptyParts);
    QJBase const* curr = this;

    for(int i=0;i<parts.size();i++)
    {
        auto & p = parts[i];
        if( p=="") continue;
        if( auto * arr = dynamic_cast<QJObject const* >(curr))
        {
            curr = arr->getItem(p);
        }
        else if ( auto * arr1 = dynamic_cast<QJArray const*>(curr) )
        {
            curr = arr1->getItem(p);
        }
        else
        {
            return nullptr;
        }
    }
    return curr;
}

inline void QJArray::setValue( QJsonValue const &value )
{
    if( value.isArray() )
    {
        auto count = this->itemCount();
        int i=0;
        auto arr = value.toArray();
        auto size  = arr.size();

        while(itemCount()>size)
        {
            pop_back();
        }

        for(auto v : arr)
        {
            if( i < count )
            {
                this->itemAt(i)->setValue(v);
            }
            else
            {
                if( m_additionalItemsOneOf.size())
                {
                    push_back( createItem(m_additionalItemsOneOf.front() ) );
                    itemAt(i)->setValue(v);
                    ++i;
                    continue;
                }
                return;
            }
            ++i;
        }
    }
}

inline void QJObject::setSchema(const QJsonObject &J)
{
    {
        auto mI = J.find("ui:spacing");
        if( mI != J.end() )
        {
            setSpacing( mI->toInt() );
        }
    }
    {
        auto mI = J.find("ui:margin");
        if( mI != J.end() )
        {
            setMargin(mI->toInt() );
        }
    }
    {
        auto mI = J.find("oneOf");
        if( mI != J.end() )
        {
            auto arr = mI->toArray();
            for(auto i=arr.begin(); i!=arr.end();i++)
            {
                addOneOf(i->toObject());
            }
            setOneOf( m_oneOf.front() );
        }
        else
        {
            addOneOf(J);
            setOneOf(J);
        }
    }
}


inline void QJObject::setOneOf(const QJsonObject &J)
{
    std::vector<QString> order;
    {
        auto mP = J.find("ui:order");
        auto p = mP->toArray();
        for(auto i=p.begin(); i!=p.end();i++)
        {
            order.push_back( i->toString() );
        }
    }

    while(getItemCount())
    {
        takeItem(999999999)->deleteLater();
    }

    auto mP = J.find("properties");

    if( mP != J.end() )
    {
        auto p = mP->toObject();

        // place all the keys in the
        // set
        std::set<QString> allKeys;
        for(auto i=p.begin(); i!=p.end();i++)
        {
            allKeys.insert(i.key());
        }
        // erase all the items in the set that exist in order.
        for(auto & x : order)
            allKeys.erase(x);
        // add the remaining keys back into the order vector
        for(auto & x : allKeys)
            order.push_back(x);

        for(auto & k : order)
        {
            auto vO = p[k].toObject();

            auto title=k;
            auto type = vO.find("type")->toString();
            {
                auto t = vO.find("title");
                if( t!=vO.end() )
                {
                    title = t->toString( k );
                }
            }

            bool visible = true;
            {
                auto t = vO.find("ui:visible");
                if( t!=vO.end() )
                {
                    visible = t->toBool();
                }
            }

            if( type == "number")
            {
                auto N = new QJNumber(vO,this, getParentForm());
                addProperty(k, N, title);

                N->connect(N, &QJNumber::valueChanged, [this, N](double d)
                {
                    (void)d;
                    auto par = this->getRootParent();
                    if( auto o = dynamic_cast<QJObject*>(par))
                    {
                        emit o->widgetValueChanged(N);
                        emit o->valueChanged( N->getPath() );
                    }
                });
            }
            else if (type == "string")
            {
                auto N = new QJString(vO,this, getParentForm());
                addProperty(k, N, title);

                N->connect(N, &QJString::valueChanged, [this, N](QString const& d)
                {
                    (void)d;
                    auto par = this->getRootParent();
                    if( auto o = dynamic_cast<QJObject*>(par))
                    {
                        emit o->widgetValueChanged(N);
                        emit o->valueChanged( N->getPath() );
                    }
                });

            }
            else if (type == "boolean")
            {
                auto N = new QJBoolean(vO,this, getParentForm());
                addProperty(k, N, title);
                N->connect(N, &QJBoolean::valueChanged, [this, N](bool d)
                {
                    (void)d;
                    auto par = this->getRootParent();
                    if( auto o = dynamic_cast<QJObject*>(par))
                    {
                        emit o->widgetValueChanged(N);
                        emit o->valueChanged( N->getPath() );
                    }
                });
            }
            else if( type == "array")
            {
                auto N = new QJArray(vO, this, getParentForm());
                addProperty(k, N, title);
            }
            else if( type == "object")
            {
                auto N = new QJObject(vO, this, getParentForm());
                // If its an object, check if it has the "ui:widget"
                // property set to window.

                auto it = vO.find("ui:widget");
                QString ui_widget = "default";
                if( it->isString() )
                    ui_widget = it->toString();

                if( ui_widget == "window")
                {
                    auto c = new CollapsableWidget(this);
                    auto willbeNull = c->setWidget(N);
                    (void)willbeNull;

                    N->setParent(this);
                    N->setProperty(k);
                    c->setText(title);
                    addPropertyWidget(k, c, title,false);
                }
                else if( ui_widget == "group")
                {
                    auto c = new QGroupBox(this);

                    auto L = new QVBoxLayout();
                    L->setSpacing(1);
                    L->setMargin(0);

                    c->setLayout( L );
                    c->layout()->addWidget(N);

                    N->setParent(this);
                    N->setProperty(k);
                    c->setTitle(title);
                    addPropertyWidget(k, c, title,false);
                }
                else
                {
                    addProperty(k, N, title,true);
                }
            }


            if( !visible )
            {
                auto P = getItemLabel(k);
                P.first->setVisible(visible);
                if( P.second)
                {
                    P.second->setVisible(visible);
                }
            }
        }
    }
}



inline QJBase* getQJBase(QWidget * w)
{
    if( auto x = dynamic_cast<QJBase*>(w))
    {
        return x;
    }

    if( auto aI = dynamic_cast<CollapsableWidget*>(w) )
    {
        // get the central widget
        auto cent = aI->widget();

        return getQJBase(cent);
    }
    else if( auto aA = dynamic_cast<QJArrayItem*>(w) )
    {
        auto cent = aA->centralWidget();

        return getQJBase(cent);
    }
    else if( auto gb = dynamic_cast<QGroupBox*>(w) )
    {
        if( gb->layout()->count() )
        {
            auto L = gb->layout()->itemAt(0);

            auto b = getQJBase(L->widget());

            return b;
        }
    }
    throw std::runtime_error("No valid QJBase");
}

inline QJBase* takeQJBase(QWidget * w)
{
    if( auto x = dynamic_cast<QJBase*>(w))
    {
        return x;
    }

    if( auto aI = dynamic_cast<CollapsableWidget*>(w))
    {
        // get the central widget
        auto cent = aI->takeWidget();

        return takeQJBase(cent);
    }
    else if( auto aA = dynamic_cast<QJArrayItem*>(w) )
    {
        auto cent = aA->takeCentralWidget();

        return takeQJBase(cent);
    }
    else if( auto gb = dynamic_cast<QGroupBox*>(w) )
    {
        if( gb->layout()->count() )
        {
            auto L = gb->layout()->takeAt(0);

            auto b = takeQJBase(L->widget());

            delete L;
            return b;
        }
    }
    throw std::runtime_error("No valid QJBase");
}

}

#endif
